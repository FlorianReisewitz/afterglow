package de.freisewitz.afterglow;

import java.io.Serializable;

/**
 * Model Venue
 *
 * @author Florian Reisewitz
 * @version 05.03.2021
 */

public class Venue implements Serializable {
    private int id;
    private String name;
    private String street;
    private String zip;
    private String city;
    private String country;
    private int capacity;
    private String description;
    private String website;

    public Venue(){

    }

    @Override
    public String toString() {
        return "in:   " + city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
