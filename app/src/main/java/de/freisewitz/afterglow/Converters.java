package de.freisewitz.afterglow;

import androidx.room.TypeConverter;

import java.time.LocalDate;

/**
 * Converters
 *
 * Enthält die TypeConverter für die Klasse LocalDate, damit sie
 * durch Room/die lokale Datenbank verarbeitet werden kann
 *
 * @author Florian Reisewitz
 * @version 22.03.2021
 */

public class Converters {
    @TypeConverter
    public static String localDateToString(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        else {
            return localDate.toString();
        }
    }

    @TypeConverter
    public static LocalDate stringToLocalDate(String string) {
        if (string == null) {
            return null;
        }
        else {
            return LocalDate.parse(string);
        }
    }
}
