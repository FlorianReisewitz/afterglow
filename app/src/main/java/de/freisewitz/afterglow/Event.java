package de.freisewitz.afterglow;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 * Model Event
 *
 * @author Florian Reisewitz
 * @version 04.03.2021
 */
@Entity
public class Event implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    @Embedded(prefix="venue")
    private Venue venue;
    private LocalDate date;

    public Event(){

    }

    @Override
    public String toString() {
        name = name.replaceAll("\\(.+?\\)", " ");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formatedDate = date.format(dtf);
        return id + ". " + name + "\n\n" + "am: " + formatedDate + "\n"
                + venue + "\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
