package de.freisewitz.afterglow;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

/**
 * EventDatabase
 *
 * @author Florian Reisewitz
 * @version 19.03.2021
 */

@Database(entities = {Event.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class EventDatabase extends RoomDatabase {
    public abstract EventDAO getEventDAO();

    private static EventDatabase database;

    public static EventDatabase getInstance(Context context){
        if(database == null){
            database = Room.databaseBuilder(context, EventDatabase.class, "event.db").allowMainThreadQueries().build();
        }
        return database;
    }
}
