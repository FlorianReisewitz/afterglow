package de.freisewitz.afterglow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * EventAdapter
 *
 * @author Florian Reisewitz
 * @version 10.03.2021
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    /*
        Das Interface OnEventClickListener ist implementiert, um auf Klicks auf eine einzelne
        CardView zu reagieren. Das Interface wird in der EventListActivity instanziert und dort
        wird dann die onEventClick überschrieben, bzw. mit seiner Funktionalität versehen, nämlich
        auf die EventDetailsActivity zu leiten mit der Instanz vom Event aus der geklickten CardView.
     */
    public interface OnEventClickListener{
        void onEventClick(Event event);
    }

    private OnEventClickListener eventClickListener;

    public void setOnEventClickListener(OnEventClickListener eventClickListener){
        this.eventClickListener = eventClickListener;
    }

    // enhält die Liste mit den Event-Daten
    private List<Event> events;
    private Context context;

    /**
     * Konstruktor
     *
     * @param events die darzustellenden Daten
     * @param context Der Context, wichtig für die Funktionalität des "Speichern-in-Favoriten"-Icon/Button
     */
    public EventAdapter(List<Event> events, Context context) {
        this.events = events;
        this.context = context;
    }

    /**
     * RecyclerView ruft diese Methode auf, um die Anzahl der Einträge abzurufen.
     * RecyclerView verwendet dies, um festzustellen, wann keine Elemente mehr angezeigt werden können.
     *
     * @return Anzahl der Einträge
     */
    @Override
    public int getItemCount() {
        return events.size();
    }

    /**
     * RecyclerView.ViewHolder
     *
     * Mit dem ViewHolder kann auf die View-Elemente zugegriffen werden, ohne nachschlagen zu müssen.
     * Der ViewHolder enthält die View.
     * Implementiert einen Listener, um auf den "Speichern-in-Favoriten"-Icon/Button reagieren zu können.
     */
    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView eventName;
        ImageButton buttonEventSaveAsFavorite;

        public EventViewHolder(View itemView, OnEventClickListener eventClickListener) {
            super(itemView);
            eventName = itemView.findViewById(R.id.textViewEventName);

            buttonEventSaveAsFavorite = itemView.findViewById(R.id.buttonFavoriteEvent);
            buttonEventSaveAsFavorite.setOnClickListener(this);

            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                eventClickListener.onEventClick(events.get(position));
            });
        }

        /**
         * Reagiert auf den "Speichern-in-Favoriten"-Icon/Button. Holt sich die Position der geklickten
         * CardView und den dazugehörigen Event. Speichert diesen Event dauerhaft in der Room-Datenbank.
         * Setzt einen Toast, der über das erfolgte Speichern informiert.
         *
         * @param view die umgebende View
         */
        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Event event = events.get(position);
            EventDatabase db = EventDatabase.getInstance(context);
            EventDAO dao = db.getEventDAO();
            dao.create(event);
            Toast.makeText(context, R.string.toast_for_event_saved_succecsfully, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * RecyclerView ruft diese Methode immer dann auf, wenn ein neuer ViewHolder erstellt werden muss.
     * Die Methode erstellt und initialisiert den ViewHolder und die zugehörige Ansicht,
     * füllt jedoch nicht den Inhalt der Ansicht aus.
     * Der ViewHolder wurde noch nicht an bestimmte Daten gebunden.
     *
     * @param parent die ViewGroup, zu der die neue Ansicht hinzugefügt wird,
     *               nachdem sie an eine Adapterposition gebunden wurde.
     * @param viewType der Ansichtstyp der neuen Ansicht.
     * @return der generierte ViewHolder
     */
    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_recyclerview_item, parent, false);
        return new EventViewHolder(view, eventClickListener);
    }

    /**
     * RecyclerView ruft diese Methode auf, um einen ViewHolder mit Daten zu verknüpfen.
     * Die Methode ruft die entsprechenden Daten ab und verwendet die Daten, um das Layout
     * des Ansichtsinhabers auszufüllen.
     *
     * @param holder der ViewHolder, der aktualisiert werden soll, um den Inhalt des
     *               Elements an der angegebenen Position im Datensatz darzustellen.
     * @param position die Position des Elements im Datensatz des Adapters.
     */
    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = events.get(position);
        holder.eventName.setText(event.getName());
    }
}
