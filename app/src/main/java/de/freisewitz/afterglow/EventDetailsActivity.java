package de.freisewitz.afterglow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.format.DateTimeFormatter;

public class EventDetailsActivity extends AppCompatActivity {
    Event event = new Event();
    EventDatabase db = EventDatabase.getInstance(this);
    EventDAO dao = db.getEventDAO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        Intent intent = getIntent();

        /*
            Überprüft, ob die vorherige EventListActivity eine Instanz der Klasse Event mitgeschickt
            hat. Wenn vorhanden, werden die TextViews mit den vorhandenen Details des Events gesetzt.
         */
        if (intent != null && intent.hasExtra("event")) {
            event = (Event) intent.getSerializableExtra("event");
            TextView textViewHeadline = findViewById(R.id.textViewEventDetailsHeadline);
            textViewHeadline.setText(event.getName().replaceAll("\\(.+?\\)", " "));
            TextView textViewLocation = findViewById(R.id.textViewEventDetailsLocation);
            textViewLocation.setText(event.getVenue().getName());
            TextView textViewCity = findViewById(R.id.textViewEventDetailsCity);
            textViewCity.setText(event.getVenue().getCity());
            TextView textViewCountry = findViewById(R.id.textViewEventDetailsCountry);
            textViewCountry.setText(event.getVenue().getCountry());
            TextView textViewDate = findViewById(R.id.textViewEventDetailsDate);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            textViewDate.setText(event.getDate().format(dtf));
        }
    }

    /**
     * Reagiert auf den Klick des Buttons "Details" und startet eine Methode, die Details
     * der Event-Location holt.
     *
     * @param view Die umgebende View
     */
    public void showVenueDetailsOnClick(View view) {
        buildVenueDetails(event.getVenue().getName());
    }

    /**
     * Baut die URL für die API-Abfrage zusammen und startet damit den Request, um die Details
     * der Location herauszufinden. Startet die Methode updateView(), die im Layout die neuen
     * Details hinzufügt.
     *
     * @param venueName der Name der Location, zu der Details gesucht werden
     */
    private void buildVenueDetails(String venueName) {
        String url = "https://api.songkick.com/api/3.0/search/venues.json?query=" + venueName +
                "&apikey=sbsDuZSbq2Wq74ds";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToVenueDetails(response);
                        updateView();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Holt sich die Daten zur Straße, PLZ, Besucherzahl, Beschreibung und Website der Location und
     * weist sie der Instanz der Klasse Event zu.
     *
     * @param jsonObject das Ergbnis der API-Abfrage nach den Details einer Location
     * @throws JSONException
     */
    private void jsonToVenueDetails(JSONObject jsonObject) throws JSONException {
        String street = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("venue").getJSONObject(0).getString("street");
        String zip = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("venue").getJSONObject(0).getString("zip");
        Object capacityObject = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("venue").getJSONObject(0).get("capacity");
        int capacity = 0;
        if (capacityObject != null && !capacityObject.toString().equals("null")) {
            capacity = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("venue").getJSONObject(0).getInt("capacity");
        }
        String description = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("venue").getJSONObject(0).getString("description");
        String website = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("venue").getJSONObject(0).getString("website");
        event.getVenue().setStreet(street);
        event.getVenue().setZip(zip);
        event.getVenue().setCapacity(capacity);
        event.getVenue().setDescription(description);
        event.getVenue().setWebsite(website);
    }

    /**
     * Überprüft, ob eines der Venue-Details überhaupt gesetzt wurde. Wenn dies der Fall ist, werden
     * die TextViews der Labels als auch der Inhalte der Venue-Details gesetzt.
     * Wenn dies nicht der Fall ist, wird ein Informations-Toast gesetzt, der aussagt, dass es
     * keine weiteren Details gibt.
     */
    private void updateView() {
        if (!event.getVenue().getStreet().equals("") || !event.getVenue().getZip().equals("")
                || !event.getVenue().getWebsite().equals("null") || (event.getVenue().getCapacity() != 0)
                || !event.getVenue().getDescription().equals("")) {
            TextView textViewLabelCapacity = findViewById(R.id.textViewLabelCapacity);
            textViewLabelCapacity.setText(R.string.event_details_label_capacity);
            TextView textViewCapacity = findViewById(R.id.textViewCapacity);
            if (event.getVenue().getCapacity() == 0) {
                textViewCapacity.setText(R.string.event_details_if_not_specified);
            }
            else {
                textViewCapacity.setText(String.valueOf(event.getVenue().getCapacity()));
            }
            TextView textViewLabelDescription = findViewById(R.id.textViewLabelDescription);
            textViewLabelDescription.setText(R.string.event_details_label_description);
            TextView textViewDescription = findViewById(R.id.textViewDescription);
            if (event.getVenue().getDescription().equals("")) {
                textViewDescription.setText(R.string.event_details_if_not_specified);
            }
            else {
                textViewDescription.setText(event.getVenue().getDescription());
            }
            TextView textViewLabelWebsite = findViewById(R.id.textViewLabelWebsite);
            textViewLabelWebsite.setText(R.string.event_details_label_website);
            TextView textViewWebsite = findViewById(R.id.textViewWebsite);
            if (event.getVenue().getWebsite().equals("null")) {
                textViewWebsite.setText(R.string.event_details_if_not_specified);
            }
            else {
                textViewWebsite.setText(event.getVenue().getWebsite());
            }
            TextView textViewLabelStreet = findViewById(R.id.textViewLabelStreet);
            textViewLabelStreet.setText(R.string.event_details_label_street);
            TextView textViewStreet = findViewById(R.id.textViewStreet);
            textViewStreet.setText(event.getVenue().getStreet());
            TextView textViewLabelZip = findViewById(R.id.textViewLabelZip);
            textViewLabelZip.setText(R.string.event_details_label_zip);
            TextView textViewZip = findViewById(R.id.textViewZipCode);
            textViewZip.setText(event.getVenue().getZip());
        }
        else {
            Toast.makeText(this, R.string.toast_if_no_further_details_found_to_event, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.afterglow_menu, menu);
        return true;
    }

    /**
     * Wird aufgerufen, wenn im Menü das Home-Icon geklickt wird -
     * führt zurück zur MainActivity.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemHomeClicked(MenuItem item) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Wird aufgerufen, wenn im Menü das Stern-Icon geklickt wird -
     * führt zur Seite mit den favorisierten Events.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemFavoritesClicked(MenuItem item) {
        Intent intent = new Intent(this, EventFavoritesActivity.class);
        startActivity(intent);
    }

    /**
     * Reagiert auf Klick des Buttons "Zurück zur Liste" und führt zurück zur EventListActivity, die
     * die Liste der eben gesuchten Events enthält.
     *
     * @param view die umgebende View
     */
    public void backFromDetailsToEventListActivity(View view) {
        finish();
    }

    /**
     * Reagiert auf Klick des Buttons "Zurück zur Suche" und führt zurück zur MainActivity.
     *
     * @param view die umgebende View
     */
    public void backFromDetailsToMainActivity(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Reagiert auf Klick des Buttons "Speichern" und speichert den aktuellen Event dauerhaft
     * in der Room-Datenbank. Setzt einen Toast, der über das erfolgte Speichern informiert.
     *
     * @param view die umgebende View
     */
    public void saveEventInFavorites(View view) {
        dao.create(event);
        Toast.makeText(this, R.string.toast_for_event_saved_succecsfully, Toast.LENGTH_SHORT).show();
    }
}