package de.freisewitz.afterglow;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/**
 * EventDAO
 *
 * @author Florian Reisewitz
 * @version 19.03.2021
 */

@Dao
public interface EventDAO {
    @Insert
    void create(Event... event);

    @Delete
    void delete(Event... event);

    @Query("SELECT * FROM Event")
    List<Event> getEventList();
}
