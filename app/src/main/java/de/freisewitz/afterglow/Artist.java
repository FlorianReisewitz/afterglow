package de.freisewitz.afterglow;

import java.io.Serializable;
import java.util.List;

/**
 * Model Artist
 *
 * @author Florian Reisewitz
 * @version 04.03.2021
 */

public class Artist implements Serializable {
    private int ID;
    private String name;
    private List<Event> events;

    public Artist(){

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
