package de.freisewitz.afterglow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

public class EventFavoritesActivity extends AppCompatActivity {

    private EventDatabase db;
    private EventDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_favorites);

        db = EventDatabase.getInstance(this);
        dao = db.getEventDAO();

        updateEventsListView();
    }

    /**
     * Holt sich alle Events, die in der Datenbank Room gespeichert sind und packt sie in eine
     * Liste. Baut den ArrayAdapter mit dieser Liste und verknüpft den Adapter mit der Listview
     * im Layout. Stellt einen Listener bereit, der auf Klicks einzelner Einträge in dieser
     * Listview wartet.
     */
    private void updateEventsListView(){
        List<Event> events = dao.getEventList();
        ArrayAdapter<Event> eventArrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, events
        );
        ListView listViewEvents = findViewById(R.id.listViewEventFavorites);
        listViewEvents.setAdapter(eventArrayAdapter);
        listViewEvents.setOnItemClickListener((parent, view, position, id) -> onClickDeleteEvent(parent, position));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.afterglow_menu, menu);
        return true;
    }

    /**
     * Wird aufgerufen, wenn im Menü das Home-Icon geklickt wird -
     * führt zurück zur MainActivity.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemHomeClicked(MenuItem item) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Wird aufgerufen, wenn im Menü das Stern-Icon geklickt wird -
     * führt eigentlich zur EventFavoritesActivity; hier leer, da
     *  man bereits dort ist.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemFavoritesClicked(MenuItem item){
    }

    /**
     * Identifiziert den geklickten Event und löscht ihn aus der Room-Datenbank.
     * Startet eine Methode, die die gesamte Ansicht neu lädt.
     * Setzt einen Informations-Toast, dass ein Eintrag aus der Liste gelöscht wurde.
     *
     * @param parent Die umgebende ELtern-View
     * @param position Die Position des geklickten List-Elements
     */
    public void onClickDeleteEvent(AdapterView<?> parent, int position){
        Event event = (Event) parent.getAdapter().getItem(position);
        dao.delete(event);
        updateEventsListView();
        Toast.makeText(this, R.string.toast_if_single_event_deleted_from_favorites, Toast.LENGTH_SHORT).show();
    }
}