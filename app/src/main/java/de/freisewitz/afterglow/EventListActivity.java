package de.freisewitz.afterglow;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class EventListActivity extends AppCompatActivity {

    private Artist artist = new Artist();
    EventDatabase db = EventDatabase.getInstance(this);
    EventDAO dao = db.getEventDAO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        // Objektreferenz des RecyclerView holen
        RecyclerView recyclerViewEvents = findViewById(R.id.recyclerViewEventList);

        // Bildet den vom RecyclerView benötigten LayoutManager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewEvents.setLayoutManager(layoutManager);

        // Überprüft, ob die vorherige MainActivity eine Instanz der Klasse Artist mitgeschickt hat
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("Artist")) {
                artist = (Artist) intent.getSerializableExtra("Artist");
            }
        }

        // Setzt die Headline des Layouts mit dem Namen der Band
        TextView eventListHeadline = findViewById(R.id.textViewEventListHeadline);
        eventListHeadline.setText(artist.getName());

        /*
            Bildet eine vom RecyclerView benötigte Instanz der Klasse EventAdapter, die die Daten
            (Liste mit Events) hält
         */
        EventAdapter adapter = new EventAdapter(artist.getEvents(), this);
        recyclerViewEvents.setAdapter(adapter);

        /*
            Implementiert einen Listener für Klicks auf einzelne Elemente des RecyclerView. Speichert
            den Event des geklickten Elements und leitet weiter an die EventDetailsActivity.
         */
        adapter.setOnEventClickListener(new EventAdapter.OnEventClickListener() {
            @Override
            public void onEventClick(Event event) {
                Intent intent = new Intent(getApplicationContext(), EventDetailsActivity.class);
                intent.putExtra("event", event);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.afterglow_menu, menu);
        return true;
    }

    /**
     * Wird aufgerufen, wenn im Menü das Home-Icon geklickt wird -
     * führt zurück zur MainActivity.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemHomeClicked(MenuItem item) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * Wird aufgerufen, wenn im Menü das Stern-Icon geklickt wird -
     * führt zur Seite mit den favorisierten Events.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemFavoritesClicked(MenuItem item){
        Intent intent = new Intent(this, EventFavoritesActivity.class);
        startActivity(intent);
    }

    /**
     * Reagiert auf Klick des Buttons "Zurück zur Suche" im Layout und führt zur MainActivity.
     *
     * @param view Die umgebende View
     */
    public void backToMainActivity(View view) {
        finish();
    }

    /**
     * Reagiert auf Klick des Buttons "Wikipedia" und öffnet in einem Browser die Wikipedia-Seite
     * der aktuellen Band.
     *
     *  @param view Die umgebende View
     */
    public void showWikipedia(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://de.wikipedia.org/wiki/" + artist.getName()));
        startActivity(intent);
    }

    /**
     * Reagiert auf Klick des Buttons "Songkick" und öffnet in einem Browser die Songkick-Seite
     * der aktuellen Band.
     *
     *  @param view Die umgebende View
     */
    public void showSongkick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.songkick.com/artists/" + artist.getID()));
        startActivity(intent);
    }

}