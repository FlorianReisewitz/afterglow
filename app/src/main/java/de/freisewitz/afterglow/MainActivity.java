package de.freisewitz.afterglow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Event> events = new ArrayList<>();
    private Artist artist = new Artist();
    // Konstanten, um die Unter- und Obergrenze der beiden Spinner festzulegen
    private final int BEGINNING_YEAR_EVENTS = 1960;
    private final int ACTUAL_YEAR = new GregorianCalendar().get(Calendar.YEAR);
    // Konstanten für die Seitenzahl der ResultPages der API-Abfragen
    private final int INDEX_SEARCH_RESULT_FIRST_PAGE = 1;
    private final int INDEX_SEARCH_RESULT_SECOND_PAGE = 2;
    // Konstante für den Zugang zur API
    private final String API_KEY = "sbsDuZSbq2Wq74ds";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        fillSpinnersWithYears();

        EventDatabase db = EventDatabase.getInstance(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.afterglow_menu, menu);
        return true;
    }

    /**
     * Wird aufgerufen, wenn im Menü das Home-Icon geklickt wird -
     * führt eigentlich zurück zur MainActivity; hier leer, da
     * man bereits in der MainActivity ist.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemHomeClicked(MenuItem item) {
    }

    /**
     * Wird aufgerufen, wenn im Menü das Stern-Icon geklickt wird -
     * führt zur Seite mit den favorisierten Events.
     *
     * @param item das geklickte Menü-Item
     */
    public void actionOnItemFavoritesClicked(MenuItem item) {
        Intent intent = new Intent(this, EventFavoritesActivity.class);
        startActivity(intent);
    }

    /**
     * Wird mit Klick auf den "Suche Events" Button aufgerufen;
     * überprüft, ob die CheckBoxes gesetzt wurden, holt sich die Werte
     * aus beiden Spinnern mit den Jahreszahlen und entscheidet je nach
     * den gesetzten Checkboxes, welche Suchabfrage losgeschickt wird.
     */
    public void searchEventsOnClick(View view) {
        events.clear();
        EditText editTextSearchTerm = findViewById(R.id.editTextArtistSearch);
        String searchTerm = editTextSearchTerm.getText().toString();

        CheckBox checkBoxSearchOptionPeriodStart = findViewById(R.id.checkboxSearchOptionPeriodStart);
        CheckBox checkBoxSearchOptionPeriodEnd = findViewById(R.id.checkboxSearchOptionPeriodEnd);
        Spinner spinnerSearchOptionPeriodStart = findViewById(R.id.spinnerSearchOptionPeriodStart);
        int selectedYearPeriodStart = Integer.valueOf(spinnerSearchOptionPeriodStart.getSelectedItem().toString());
        Spinner spinnerSearchOptionPeriodEnd = findViewById(R.id.spinnerSearchOptionPeriodEnd);
        int selectedYearPeriodEnd = Integer.valueOf(spinnerSearchOptionPeriodEnd.getSelectedItem().toString());

        if (!searchTerm.equals("")) {
            if (!checkBoxSearchOptionPeriodStart.isChecked() && !checkBoxSearchOptionPeriodEnd.isChecked()) {
                buildArtist(searchTerm);
            }
            else if (checkBoxSearchOptionPeriodStart.isChecked() && !checkBoxSearchOptionPeriodEnd.isChecked()) {
                buildArtist(searchTerm, selectedYearPeriodStart);
            }
            else if (!checkBoxSearchOptionPeriodStart.isChecked() && checkBoxSearchOptionPeriodEnd.isChecked()) {
                buildArtist(searchTerm, selectedYearPeriodEnd);
            }
            else {
                buildArtist(searchTerm, selectedYearPeriodStart, selectedYearPeriodEnd);
            }
        }
        else {
            Toast.makeText(this, R.string.toast_if_edit_text_band_is_empty, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Baut die URL für die API-Abfrage zusammen und startet damit den Request, um die ID
     * der Band herauszufinden; startet einen weiteren Request, um die Events zu dieser Band
     * zu finden.
     *
     * @param searchTerm Der Name der Band, der in das Suchfeld eingegeben wurde
     */
    private void buildArtist(String searchTerm) {
        String url = "https://api.songkick.com/api/3.0/search/artists.json?apikey=" + API_KEY + "&query=" + searchTerm;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToArtist(response);
                        getEvents();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(this, R.string.toast_if_typo_in_bandname, Toast.LENGTH_SHORT).show();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URL für die API-Abfrage zusammen und startet damit den Request, um die ID
     * der Band herauszufinden; startet einen weiteren Request, um die Events zu dieser Band
     * zu finden.
     *
     * @param searchTerm Der Name der Band, der in das Suchfeld eingegeben wurde
     * @param year       Das Jahr, für das Events gesucht werden
     */
    private void buildArtist(String searchTerm, int year) {
        String url = "https://api.songkick.com/api/3.0/search/artists.json?apikey=" + API_KEY + "&query=" + searchTerm;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToArtist(response);
                        getEvents(year);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(this, R.string.toast_if_typo_in_bandname, Toast.LENGTH_SHORT).show();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URL für die API-Abfrage zusammen und startet damit den Request, um die ID
     * der Band herauszufinden; startet einen weiteren Request, um die Events zu dieser Band
     * zu finden.
     *
     * @param searchTerm  Der Name der Band, der in das Suchfeld eingegeben wurde
     * @param periodStart Das Jahr des Zeitabschnitts,  ab dem Events gesucht werden
     * @param periodEnd   Das Jahr des Zeitabschnitts,  bis zu dem Events gesucht werden
     */
    private void buildArtist(String searchTerm, int periodStart, int periodEnd) {
        String url = "https://api.songkick.com/api/3.0/search/artists.json?apikey=" + API_KEY + "&query=" + searchTerm;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToArtist(response);
                        getEvents(periodStart, periodEnd);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(this, R.string.toast_if_typo_in_bandname, Toast.LENGTH_SHORT).show();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band zu finden, die auf der ersten ResultPage aufgelistet sind.
     * Überprüft, wieviele Events insgesamt vorhanden sind; startet eine weitere API-Abfrage,
     * wenn mehr als 100 Events vorhanden sind; wenn 100 oder weniger Events vorhanden sind,
     * wird eine Methode gestartet, die mit dem fertigen Suchergebnis eine neue Seite startet.
     */
    private void getEvents() {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_FIRST_PAGE + "&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        int totalEntries = response.getJSONObject("resultsPage").getInt("totalEntries");
                        if (totalEntries > 100) {
                            getFurtherEvents();
                        }
                        else {
                            showEventListFromArtist();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band zu finden, die auf der zweiten ResultPage aufgelistet sind.
     * <p>
     * Startet eine Methode, die mit dem fertigen Suchergebnis eine neue Seite startet.
     */
    private void getFurtherEvents() {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_SECOND_PAGE + "&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        showEventListFromArtist();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band zu finden, die in einem bestimmten Jahr stattgefunden haben und die auf der
     * ersten ResultPage aufgelistet sind.
     * Überprüft, wieviele Events insgesamt vorhanden sind; startet eine weitere API-Abfrage,
     * wenn mehr als 100 Events vorhanden sind; wenn 100 oder weniger Events vorhanden sind,
     * wird eine Methode gestartet, die mit dem fertigen Suchergebnis eine neue Seite startet.
     *
     * @param year Das Jahr, für das Events gesucht werden
     */
    private void getEvents(int year) {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_FIRST_PAGE +
                "&min_date=" + year + "-01-01&max_date=" + year + "-12-31&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        int totalEntries = response.getJSONObject("resultsPage").getInt("totalEntries");
                        if (totalEntries > 100) {
                            getFurtherEvents(year);
                        }
                        else {
                            showEventListFromArtist();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band in einem bestimmten Jahr zu finden, die auf der zweiten ResultPage aufgelistet sind.
     * <p>
     * Startet eine Methode, die mit dem fertigen Suchergebnis eine neue Seite startet.
     *
     * @param year Das Jahr, für das Events gesucht werden
     */
    private void getFurtherEvents(int year) {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_SECOND_PAGE +
                "&min_date=" + year + "-01-01&max_date=" + year + "-12-31&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        showEventListFromArtist();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band zu finden, die in einer bestimmten Periode von Jahren stattgefunden haben
     * und die auf der ersten ResultPage aufgelistet sind.
     * Überprüft, wieviele Events insgesamt vorhanden sind; startet eine weitere API-Abfrage,
     * wenn mehr als 100 Events vorhanden sind; wenn 100 oder weniger Events vorhanden sind,
     * wird eine Methode gestartet, die mit dem fertigen Suchergebnis eine neue Seite startet.
     *
     * @param periodStart Das Jahr des Zeitabschnitts,  ab dem Events gesucht werden
     * @param periodEnd   Das Jahr des Zeitabschnitts,  bis zu dem Events gesucht werden
     */
    private void getEvents(int periodStart, int periodEnd) {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_FIRST_PAGE +
                "&min_date=" + periodStart + "-01-01&max_date=" + periodEnd + "-12-31&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        int totalEntries = response.getJSONObject("resultsPage").getInt("totalEntries");
                        if (totalEntries > 100) {
                            getFurtherEvents(periodStart, periodEnd);
                        }
                        else {
                            showEventListFromArtist();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Baut die URl für die API-Abfrage zusammen und startet damit den Request, um alle Events
     * zu einer Band in einer bestimmten Periode von Jahren zu finden, die auf der zweiten
     * ResultPage aufgelistet sind.
     * <p>
     * Startet eine Methode, die mit dem fertigen Suchergebnis eine neue Seite startet.
     *
     * @param periodStart Das Jahr des Zeitabschnitts,  ab dem Events gesucht werden
     * @param periodEnd   Das Jahr des Zeitabschnitts,  bis zu dem Events gesucht werden
     */
    private void getFurtherEvents(int periodStart, int periodEnd) {
        String url = "https://api.songkick.com/api/3.0/artists/" + artist.getID() +
                "/gigography.json?apikey=" + API_KEY + "&page=" + INDEX_SEARCH_RESULT_SECOND_PAGE +
                "&min_date=" + periodStart + "-01-01&max_date=" + periodEnd + "-12-31&per_page=100";
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                response -> {
                    try {
                        jsonToEvents(response);
                        showEventListFromArtist();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> Log.e("JSON", error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    /**
     * Weist die EventListe der Instanz der Klasse Artist zu, und wechselt unter Mitnahme dieser
     * Instanz auf eine andere Activity, in der die EventListe angezeigt werden wird.
     */
    private void showEventListFromArtist() {
        artist.setEvents(events);
        Intent intent = new Intent(this, EventListActivity.class);
        intent.putExtra("Artist", artist);
        startActivity(intent);
    }

    /**
     * Holt die ID und den vollen Namen der Band aus dem JSONObject, dass die API-Abfrage mit dem
     * Bandnamen ergeben hat und weist sie der Instanz der Klasse Artist zu.
     *
     * @param jsonObject das Ergebnis der API-Abfrage mit dem Bandnamen
     * @throws JSONException
     */
    private void jsonToArtist(JSONObject jsonObject) throws JSONException {
        artist.setID(jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("artist").getJSONObject(0).getInt("id"));
        artist.setName(jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("artist").getJSONObject(0).getString("displayName"));
    }

    /**
     * Geht in einer For-Schleife alle gefunden Events der API-Abfrage durch, holt sich zu jedem
     * Event jeweils die Daten zum Namen des Events, Namen, Stadt & Land der Location, weist die Daten
     * der Location einer Instanz der Klasse Venue zu, weist das Venue einer Instanz der Klasse
     * Event zu, und fügt diesen Event einer Liste von Events hinzu.
     *
     * @param jsonObject das Ergbnis der API-Abfrage nach den Events einer Band
     * @throws JSONException
     */
    private void jsonToEvents(JSONObject jsonObject) throws JSONException {
        for (int i = 0; i < jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                .getJSONArray("event").length(); i++) {
            Event event = new Event();
            Venue venue = new Venue();
            String name = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("event").getJSONObject(i).getString("displayName");
            event.setName(name);
            String dateAsString = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("event").getJSONObject(i).getJSONObject("start").getString("date");
            if (dateAsString.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
                LocalDate date = LocalDate.parse(dateAsString);
                event.setDate(date);
            }
            String venueName = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("event").getJSONObject(i).getJSONObject("venue")
                    .getString("displayName");
            String city = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("event").getJSONObject(i).getJSONObject("venue")
                    .getJSONObject("metroArea").getString("displayName");
            String country = jsonObject.getJSONObject("resultsPage").getJSONObject("results")
                    .getJSONArray("event").getJSONObject(i).getJSONObject("venue")
                    .getJSONObject("metroArea").getJSONObject("country").getString("displayName");
            venue.setName(venueName);
            venue.setCity(city);
            venue.setCountry(country);
            event.setVenue(venue);
            events.add(event);
        }
    }

    /**
     * Ruft eine Methode auf, die ein Array mit Jahreszahlen füllt und setzt die Spinner, die dem
     * Benutzer eine Auswahl aus Jahren ermöglichen, auf Basis dieses Arrays zusammen.
     */
    private void fillSpinnersWithYears() {
        String[] yearsArray = buildYearsArray();

        Spinner spinnerSearchOptionPeriodStart = findViewById(R.id.spinnerSearchOptionPeriodStart);
        spinnerSearchOptionPeriodStart.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1,
                yearsArray
        ));
        Spinner spinnerSearchOptionPeriodEnd = findViewById(R.id.spinnerSearchOptionPeriodEnd);
        spinnerSearchOptionPeriodEnd.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1,
                yearsArray
        ));
    }

    /**
     * Bildet ein Array aus Jahreszahlen, beginnend mit dem aktuellen Kalenderjahr, rücklaufend bis
     * zum festgesetzten frühesten Jahr, nach dem gesucht werden kann.
     *
     * @return Ein StringArray mit Jahreszahlen
     */
    private String[] buildYearsArray() {
        String[] yearsArray = new String[ACTUAL_YEAR - BEGINNING_YEAR_EVENTS];
        for (int i = 0; i < yearsArray.length; i++) {
            yearsArray[i] = String.valueOf(ACTUAL_YEAR - i);
        }
        return yearsArray;
    }
}